package model.data_structures;

import java.util.Iterator;
public class ListaEncadenada<T> implements ILista<T> {

	public NodoSencillo<T> cabeza;
	public NodoSencillo<T> referencia;
	public int posReferencia;
	public NodoSencillo<T> ultimoElemento;
	public int cantidadElementos;

	@Override
	public Iterator<T> iterator() {

		Iterator<T> respuesta = new listaiterator<T>();
		return respuesta;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(referencia == null)
		{
			NodoSencillo<T> temp = new NodoSencillo<T>(elem);
			referencia = temp;
			ultimoElemento = temp;
			cabeza = temp;
			cantidadElementos = 1;
			posReferencia = 0;
		}
		else
		{
			NodoSencillo<T> temp = new NodoSencillo<T>(elem);
			ultimoElemento.cambiarSiguiente(temp);
			ultimoElemento = ultimoElemento.darSiguiente();
			cantidadElementos++;
		}
	}

	@Override
	public T darElemento(int pos) {

		Iterator iteradortemp = iterator();

		referencia = cabeza;
		posReferencia = 0;

		for(int g = 0; g < pos; g ++)
		{
			if(iteradortemp.hasNext())
			{
				referencia = (NodoSencillo<T>) iteradortemp.next();
				posReferencia ++;
			}
			else
			{
				break;
			}
		}
		return referencia.darElemento();
	}


	@Override
	public int darNumeroElementos() {

		return cantidadElementos;
	}

	@Override
	public T darElementoPosicionActual() {

		return referencia.darElemento();

	}

	@Override
	public boolean avanzarSiguientePosicion() {

		Iterator iteradortemp = iterator();

		if(iteradortemp.hasNext())
		{
			referencia = referencia.darSiguiente();
			posReferencia++;
			return true;
		}
		else
			return false;

	}

	@Override
	public boolean retrocederPosicionAnterior() {

		if(posReferencia != 0)
		{
			posReferencia--;
			int posi = posReferencia;
			referencia = cabeza;

			Iterator iteradortemp = iterator();

			for(int g = 0; g < posi; g ++)
			{
				referencia = (NodoSencillo<T>) iteradortemp.next();		
			}
			return true;
		}
		else
		{
			return false;
		}

	}

	private class NodoSencillo <T>
	{
		private NodoSencillo<T> siguiente;
		private T elemento;

		public NodoSencillo(T pelemento)
		{
			elemento = pelemento;
		}

		public T darElemento()
		{
			return elemento;
		}

		public NodoSencillo<T> darSiguiente()
		{
			return siguiente;
		}

		public void cambiarSiguiente(NodoSencillo<T> ssiguiente)
		{
			siguiente = ssiguiente;
		}
	}


	private class listaiterator<T> implements Iterator
	{		
		@Override
		public boolean hasNext() {
			if(referencia.darSiguiente() != null)
			{
				return true;
			}
			return false;
		}

		@Override
		public Object next() {
			if(hasNext())
			{
				return referencia.darSiguiente();
			}
			return null;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}
	}

}
