package model.data_structures;

import java.util.Iterator;
public class ListaDobleEncadenada<T> implements ILista<T> {

	public NodoDoble<T> cabeza;
	public NodoDoble<T> referencia;
	public NodoDoble<T> ultimoElemento;
	public int cantidadElementos;

	@Override
	public Iterator<T> iterator() {

		Iterator<T> respuesta = new listaiteratordoble<T>();
		return respuesta;
	}

	@Override
	public void agregarElementoFinal(T elem) {

		if(referencia == null)
		{
			NodoDoble<T> temp = new NodoDoble<T>(elem);
			referencia = temp;
			ultimoElemento = temp;
			cabeza = temp;
			cantidadElementos = 1;
		}
		else
		{
			NodoDoble<T> temp = new NodoDoble<T>(elem);
			ultimoElemento.cambiarSiguiente(temp);
			ultimoElemento.darSiguiente().cambiarAnterior(ultimoElemento);
			ultimoElemento = ultimoElemento.darSiguiente();
			cantidadElementos++;
		}	
	}

	@Override
	public T darElemento(int pos) {

		Iterator iteradortemp = iterator();
		referencia = cabeza;

		for(int g = 0; g < pos; g ++)
		{
			if(iteradortemp.hasNext())
			{
				referencia = (NodoDoble<T>) iteradortemp.next();
			}
			else
			{
				break;
			}
		}
		return referencia.darElemento();
	}


	@Override
	public int darNumeroElementos() {

		return cantidadElementos;

	}

	@Override
	public T darElementoPosicionActual() {

		return referencia.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() {

		Iterator iteradortemp = iterator();

		if(iteradortemp.hasNext())
		{
			referencia = referencia.darSiguiente();
			return true;
		}
		else
			return false;

	}

	@Override
	public boolean retrocederPosicionAnterior() {

		if(referencia.darAnterior() != null)
		{
			referencia = referencia.darAnterior();
			return true;	
		}
		else
		{
			return false;
		}

	}


	private class NodoDoble <T>
	{
		private NodoDoble<T> siguiente;
		private NodoDoble<T> anterior;
		private T elemento;

		public NodoDoble(T pelemento)
		{
			elemento = pelemento;
		}

		public T darElemento()
		{
			return elemento;
		}

		public NodoDoble<T> darSiguiente()
		{
			return siguiente;
		}

		public NodoDoble<T> darAnterior()
		{
			return anterior;
		}

		public void cambiarSiguiente(NodoDoble<T> ssiguiente)
		{
			siguiente = ssiguiente;
		}

		public void cambiarAnterior(NodoDoble<T> aanterior)
		{
			anterior = aanterior;
		}
	}


	private class listaiteratordoble<T> implements Iterator
	{		
		@Override
		public boolean hasNext() {
			if(referencia.darSiguiente() != null)
			{
				return true;
			}
			return false;
		}

		@Override
		public Object next() {
			if(hasNext())
			{
				return referencia.darSiguiente();
			}
			return null;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}
	}

}
