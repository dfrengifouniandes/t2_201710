package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {

		BufferedReader lector = null;
		FileReader reader = null;

		misPeliculas = new ListaEncadenada<VOPelicula>();
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();

		for(int pos = 1950; pos < 2017; pos++)
		{
			VOAgnoPelicula peliano = new VOAgnoPelicula();
			peliano.setAgno(pos);
			peliculasAgno.agregarElementoFinal(peliano);
		}

		try
		{
			reader = new FileReader(archivoPeliculas);
			lector = new BufferedReader(reader);

			String linea = lector.readLine();

			while(linea != null)
			{

				VOPelicula peliculatemp = new VOPelicula();

				String[] arreglo = linea.split(",");

				//titulo
				String titulo = "";

				for(int pos = 1; pos < arreglo.length - 1; pos++)
				{
					titulo += arreglo[pos];
				}
				peliculatemp.setTitulo(titulo);

				//generos
				String[] genres = arreglo[arreglo.length - 1].split("|");

				ILista<String> tempo = new ListaDobleEncadenada<String>();

				for(int pos = 0; pos < genres.length; pos++)
				{
					tempo.agregarElementoFinal(genres[pos]);
				}
				peliculatemp.setGenerosAsociados(tempo);

				//anio
				String[] anio = arreglo[arreglo.length - 2].split("(");
				String hola = anio[anio.length - 1];
				String boleo = "" + hola.charAt(0) + hola.charAt(1) + hola.charAt(2)+ hola.charAt(3);
				peliculatemp.setAgnoPublicacion(Integer.parseInt(boleo));

				//meterla en mis peliculas
				misPeliculas.agregarElementoFinal(peliculatemp);

				//meterlas en lista agno pelicula
				VOAgnoPelicula elemento = peliculasAgno.darElemento(Integer.parseInt(boleo) - 1950);
				ILista<VOPelicula> peliculass = elemento.getPeliculas();
				peliculass.agregarElementoFinal(peliculatemp);
				elemento.setPeliculas(peliculass);

				linea = lector.readLine();
			}

		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try {

				if (reader != null)
					reader.close();

				if (lector != null)
					lector.close();

			} catch (IOException ex) {

				ex.printStackTrace();
			}
		}
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ILista<VOPelicula> respuesta = new ListaEncadenada<VOPelicula>();

		for(VOPelicula pelicula : misPeliculas)
		{
			if(pelicula.getTitulo().contains(busqueda))
			{
				respuesta.agregarElementoFinal(pelicula);
			}
		}
		return respuesta;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {

		return peliculasAgno.darElemento(agno - 1950).getPeliculas();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {

		if(peliculasAgno.avanzarSiguientePosicion() == true)
		{
			return peliculasAgno.darElementoPosicionActual();
		}
		else
		{
			return peliculasAgno.darElementoPosicionActual();
		}
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {

		if(peliculasAgno.retrocederPosicionAnterior() == true)
		{
			return peliculasAgno.darElementoPosicionActual();
		}
		else
		{
			return peliculasAgno.darElementoPosicionActual();
		}
	}

}
